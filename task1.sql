SELECT model, speed, hd
FROM PC
WHERE price < 500

SELECT DISTINCT maker
FROM Product
WHERE type = 'Printer'

SELECT model, ram, screen
FROM Laptop
WHERE price > 1000

SELECT * FROM Printer
WHERE color = 'y'

SELECT model, speed, hd
FROM PC
WHERE ((cd = '12x') OR (cd = '24x')) AND (price < 600)

SELECT DISTINCT maker, speed
FROM Product INNER JOIN Laptop ON Product.model = Laptop.model
WHERE hd >= 10

SELECT Product.model, price
FROM Product INNER JOIN
	PC ON (PC.model = Product.model) AND (Product.maker = 'B')
UNION
SELECT Product.model, price
FROM Product INNER JOIN
	Laptop ON (Laptop.model = Product.model) AND (Product.maker = 'B')
UNION
SELECT Product.model, price
FROM Product INNER JOIN
	Printer ON (Printer.model = Product.model) AND (Product.maker = 'B')

SELECT maker FROM Product WHERE type = 'PC'
EXCEPT
SELECT maker FROM Product WHERE type = 'Laptop'

SELECT DISTINCT maker
FROM Product INNER JOIN
	PC ON (Product.model = PC.model) AND (PC.speed >= 450)

SELECT model, price
FROM Printer
WHERE price = (SELECT MAX(price) FROM Printer)

SELECT AVG(speed) FROM PC

SELECT AVG(speed)
FROM Laptop
WHERE price > 1000

SELECT AVG(speed)
FROM Product INNER JOIN
	PC ON PC.model = Product.model
WHERE Product.maker = 'A'

SELECT DISTINCT maker, type
FROM Product
WHERE maker IN ( SELECT maker
				 FROM Product
				 GROUP BY maker
				 HAVING COUNT(*) > 1 AND COUNT(DISTINCT type) = 1
				)

SELECT hd
FROM PC
GROUP BY hd
HAVING COUNT(*) >= 2
