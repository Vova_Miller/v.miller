/* ### READ UNCOMMITTED ### */
/* Потерянные изменения */
/*1*/ SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
/*1*/ BEGIN TRANSACTION
/*3*/ UPDATE Broadcasting SET Frequency = Frequency + 5 WHERE SatelliteID = 6
/*3*/ COMMIT

/* Грязное чтение */
/*1*/ SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
/*1*/ BEGIN TRANSACTION
/*3*/ UPDATE Broadcasting SET Frequency = Frequency + 10 WHERE SatelliteID = 6
/*5*/ ROLLBACK


/* ### READ COMMITTED ### */
/* Грязное чтение */
/*1*/ SET TRANSACTION ISOLATION LEVEL READ COMMITTED
/*1*/ BEGIN TRANSACTION
/*3*/ UPDATE Broadcasting SET Frequency = Frequency + 10 WHERE SatelliteID = 6
/*5*/ ROLLBACK

/* Неповторяющееся чтение */
/*1*/ SET TRANSACTION ISOLATION LEVEL READ COMMITTED
/*1*/ BEGIN TRANSACTION
/*3*/ UPDATE Broadcasting SET Frequency = Frequency + 10 WHERE SatelliteID = 6
/*3*/ COMMIT


/* ### REPEATABLE READ ### */
/* Неповторяющееся чтение */
/*1*/ SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
/*1*/ BEGIN TRANSACTION
/*3*/ UPDATE Broadcasting SET Frequency = Frequency + 10 WHERE SatelliteID = 6
/*3*/ COMMIT

/* Фантомы */
/*1*/ SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
/*1*/ BEGIN TRANSACTION
/*3*/ INSERT INTO Broadcasting VALUES (6, 19, 75, 3, 4)
/*3*/ COMMIT


/* ### SERIALIZABLE ### */
/* Фантомы */
/*1*/ SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
/*1*/ BEGIN TRANSACTION
/*3*/ INSERT INTO Broadcasting VALUES (6, 19, 75, 3, 4)
/*3*/ COMMIT