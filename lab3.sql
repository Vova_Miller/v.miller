/* Удалить таблицы, если существовали */
IF OBJECT_ID('CompanyChannel', 'U') IS NOT NULL
	DROP TABLE CompanyChannel;
IF OBJECT_ID('ChannelSpecifics', 'U') IS NOT NULL
	DROP TABLE ChannelSpecifics;
IF OBJECT_ID('Specifics', 'U') IS NOT NULL
	DROP TABLE Specifics;
IF OBJECT_ID('Broadcasting', 'U') IS NOT NULL
	DROP TABLE Broadcasting;
IF OBJECT_ID('Satellite', 'U') IS NOT NULL
	DROP TABLE Satellite;
IF OBJECT_ID('Channel', 'U') IS NOT NULL
	DROP TABLE Channel;
IF OBJECT_ID('Company', 'U') IS NOT NULL
	DROP TABLE Company;
IF OBJECT_ID('Country', 'U') IS NOT NULL
	DROP TABLE Country;


/* Создание таблиц */
CREATE TABLE Satellite (
	ID int NOT NULL,
	CountryID int NOT NULL,
	Name varchar(64),
	LifeTime date,
	OrbitRadius int,
	CONSTRAINT PK_Satellite PRIMARY KEY (ID ASC)
);

CREATE TABLE Channel (
	ID int NOT NULL,
	Name varchar(64),
	Language varchar(64),
	CONSTRAINT PK_Channel PRIMARY KEY (ID ASC)
);

CREATE TABLE Broadcasting (
	SatelliteID int NOT NULL,
	ChannelID int NOT NULL,
	Frequency int NOT NULL,  -- МГц
	ZoneFrom int NOT NULL,  -- [1, 7]
	ZoneTo int NOT NULL,  -- [1, 7]
	CONSTRAINT PK_Broadcasting PRIMARY KEY (SatelliteID ASC, ChannelID ASC)
);

CREATE TABLE Company (
	ID int NOT NULL,
	CountryID int NOT NULL,
	Name varchar(64),
	CONSTRAINT PK_Company PRIMARY KEY (ID ASC)
)

CREATE TABLE Specifics (
	ID int NOT NULL,
	Description varchar(64),
	CONSTRAINT PK_Specifics PRIMARY KEY (ID ASC)
)

CREATE TABLE Country (
	ID int NOT NULL,
	Name varchar(64),
	CONSTRAINT PK_Country PRIMARY KEY (ID ASC)
)

CREATE TABLE CompanyChannel (
	CompanyID int NOT NULL,
	ChannelID int NOT NULL,
	CONSTRAINT PK_CompanyChannel PRIMARY KEY (CompanyID ASC, ChannelID ASC)
)

CREATE TABLE ChannelSpecifics (
	ChannelID int NOT NULL,
	SpecificsID int NOT NULL,
	CONSTRAINT PK_ChannelSpecifics PRIMARY KEY (ChannelID ASC, SpecificsID ASC)
)


/* Создание отношений */
ALTER TABLE Satellite
ADD CONSTRAINT FK_Satellite_Country FOREIGN KEY (CountryID)
REFERENCES Country(ID);

ALTER TABLE Broadcasting
ADD CONSTRAINT FK_Broadcasting_Satellite FOREIGN KEY (SatelliteID)
REFERENCES Satellite(ID);

ALTER TABLE Broadcasting
ADD CONSTRAINT FK_Broadcasting_Channel FOREIGN KEY (ChannelID)
REFERENCES Channel(ID);

ALTER TABLE Broadcasting
ADD CONSTRAINT CK_Zone
CHECK ((ZoneFrom >= 1) AND (ZoneTo <= 7) AND (ZoneFrom <= ZoneTo));

ALTER TABLE Company
ADD CONSTRAINT FK_Company_Country FOREIGN KEY (CountryID)
REFERENCES Country(ID);

ALTER TABLE ChannelSpecifics
ADD CONSTRAINT FK_ChannelSpecifics_Specifics FOREIGN KEY (SpecificsID)
REFERENCES Specifics(ID);

ALTER TABLE ChannelSpecifics
ADD CONSTRAINT FK_ChannelSpecifics_Channel FOREIGN KEY (SpecificsID)
REFERENCES Channel(ID);

ALTER TABLE CompanyChannel
ADD CONSTRAINT FK_CompanyChannel_Channel FOREIGN KEY (ChannelID)
REFERENCES Channel(ID);

ALTER TABLE CompanyChannel
ADD CONSTRAINT FK_CompanyChannel_Company FOREIGN KEY (CompanyID)
REFERENCES Company(ID);

/* Очищение таблиц (на всякий случай) */
DELETE FROM Satellite
DELETE FROM Channel
DELETE FROM Broadcasting;
DELETE FROM Company
DELETE FROM Specifics
DELETE FROM Country
DELETE FROM CompanyChannel
DELETE FROM ChannelSpecifics


/* Заполнение таблиц */
INSERT Channel (ID, Name, Language) VALUES (1, 'First Channel', 'Russian')
INSERT Channel (ID, Name, Language) VALUES (2, 'NTV', 'Russian')
INSERT Channel (ID, Name, Language) VALUES (3, 'Culture', 'Russian')
INSERT Channel (ID, Name, Language) VALUES (4, 'TNT', 'Russian')
INSERT Channel (ID, Name, Language) VALUES (5, 'REN TV', 'Russian')
INSERT Channel (ID, Name, Language) VALUES (6, '2х2', 'Russian')
INSERT Channel (ID, Name, Language) VALUES (7, 'Discovery', 'English')
INSERT Channel (ID, Name, Language) VALUES (8, 'Nickelodeon', 'English')
INSERT Channel (ID, Name, Language) VALUES (9, 'K1', 'English')
INSERT Channel (ID, Name, Language) VALUES (10, 'Yazuki 2000', 'Japanese')
INSERT Channel (ID, Name, Language) VALUES (11, 'Mizuki TV', 'Japanese')
INSERT Channel (ID, Name, Language) VALUES (12, 'LTV1', 'Latvian')
INSERT Channel (ID, Name, Language) VALUES (13, 'TV3', 'Latvian')
INSERT Channel (ID, Name, Language) VALUES (14, 'LNT', 'Latvian')
INSERT Channel (ID, Name, Language) VALUES (15, 'Inter+', 'Ukrainian')
INSERT Channel (ID, Name, Language) VALUES (16, 'Espreso tv', 'Ukrainian')
INSERT Channel (ID, Name, Language) VALUES (17, 'Kazakh TV', 'Kazakhstan')
INSERT Channel (ID, Name, Language) VALUES (18, 'France24', 'French')
INSERT Channel (ID, Name, Language) VALUES (19, 'CNC World', 'Chinese')
INSERT Channel (ID, Name, Language) VALUES (20, 'Disney', 'German')
INSERT Channel (ID, Name, Language) VALUES (21, 'Deluxe Music', 'German')
INSERT Channel (ID, Name, Language) VALUES (22, 'sixx', 'German')

INSERT Country (ID, Name) VALUES (1, 'Russia')
INSERT Country (ID, Name) VALUES (2, 'England')
INSERT Country (ID, Name) VALUES (3, 'USA')
INSERT Country (ID, Name) VALUES (4, 'Japan')
INSERT Country (ID, Name) VALUES (5, 'Latvia')
INSERT Country (ID, Name) VALUES (6, 'Ukraine')
INSERT Country (ID, Name) VALUES (7, 'Kazakhstan')
INSERT Country (ID, Name) VALUES (8, 'France')
INSERT Country (ID, Name) VALUES (9, 'China')
INSERT Country (ID, Name) VALUES (10, 'Germany')
INSERT Country (ID, Name) VALUES (11, 'Egypt')
INSERT Country (ID, Name) VALUES (12, 'Italy')

INSERT Company (ID, CountryID, Name) VALUES (1, 1, 'VGTRK')
INSERT Company (ID, CountryID, Name) VALUES (2, 1, 'Gazprom Media Holding')
INSERT Company (ID, CountryID, Name) VALUES (3, 2, 'British Media')
INSERT Company (ID, CountryID, Name) VALUES (4, 4, 'Japan TV Company')
INSERT Company (ID, CountryID, Name) VALUES (5, 5, 'CIS Media Holding')
INSERT Company (ID, CountryID, Name) VALUES (6, 3, 'USA TV 2000')
INSERT Company (ID, CountryID, Name) VALUES (7, 11, 'Egypt Pro TV')
INSERT Company (ID, CountryID, Name) VALUES (8, 8, 'France Advanced Media')
INSERT Company (ID, CountryID, Name) VALUES (9, 9, 'East TV World')
INSERT Company (ID, CountryID, Name) VALUES (10, 10, 'German Modern Media')
INSERT Company (ID, CountryID, Name) VALUES (11, 6, 'Ukraine Independent Holding')

INSERT Satellite (ID, CountryID, Name, LifeTime, OrbitRadius) VALUES (1, 1, 'Union', '2020-12-31', 46237)
INSERT Satellite (ID, CountryID, Name, LifeTime, OrbitRadius) VALUES (2, 1, 'CIS', '2019-01-01', 42164)
INSERT Satellite (ID, CountryID, Name, LifeTime, OrbitRadius) VALUES (3, 11, 'African bit', '2040-12-31', 47424)
INSERT Satellite (ID, CountryID, Name, LifeTime, OrbitRadius) VALUES (4, 10, 'Europe TV connector', '2025-01-01', 41634)
INSERT Satellite (ID, CountryID, Name, LifeTime, OrbitRadius) VALUES (5, 9, 'China Space', '2027-12-31', 42153)
INSERT Satellite (ID, CountryID, Name, LifeTime, OrbitRadius) VALUES (6, 4, 'Japan Space World', '2050-01-01', 45137)
INSERT Satellite (ID, CountryID, Name, LifeTime, OrbitRadius) VALUES (7, 3, 'Apollo', '2043-12-31', 41333)
INSERT Satellite (ID, CountryID, Name, LifeTime, OrbitRadius) VALUES (8, 1, 'ISS', '2039-01-01', 43513)
INSERT Satellite (ID, CountryID, Name, LifeTime, OrbitRadius) VALUES (9, 2, 'European Satellite', '2026-12-31', 43278)
INSERT Satellite (ID, CountryID, Name, LifeTime, OrbitRadius) VALUES (10, 9, 'Eastern Satellite', '2022-01-01', 42672)

INSERT Specifics (ID, Description) VALUES (1, 'Humor')
INSERT Specifics (ID, Description) VALUES (2, 'News')
INSERT Specifics (ID, Description) VALUES (3, 'Shop')
INSERT Specifics (ID, Description) VALUES (4, 'Films')
INSERT Specifics (ID, Description) VALUES (5, 'TV series')
INSERT Specifics (ID, Description) VALUES (6, 'Music')
INSERT Specifics (ID, Description) VALUES (7, 'Documentary')
INSERT Specifics (ID, Description) VALUES (8, 'Animation')
INSERT Specifics (ID, Description) VALUES (9, 'Retro')
INSERT Specifics (ID, Description) VALUES (10, 'Sport')

INSERT INTO Broadcasting
VALUES	(1, 1, 50, 1, 3),
		(1, 2, 80, 1, 3),
		(1, 3, 110, 1, 3),
		(1, 4, 140, 1, 3),
		(1, 5, 170, 1, 3),
		(1, 6, 200, 1, 3),
		(2, 12, 60, 3, 4),
		(2, 15, 100, 3, 4),
		(2, 17, 140, 3, 4),
		(4, 13, 55, 3, 3),
		(4, 16, 77, 3, 3),
		(4, 20, 98, 3, 3),
		(5, 19, 101, 3, 4),
		(6, 11, 67, 3, 3),
		(7, 7, 59, 2, 4),
		(7, 8, 76, 2, 4),
		(9, 9, 65, 3, 3),
		(9, 14, 95, 3, 3),
		(9, 18, 125, 3, 3),
		(9, 21, 155, 3, 3),
		(9, 22, 185, 3, 3),
		(10, 10, 140, 3, 3);

INSERT INTO CompanyChannel
VALUES	(1, 1),
		(1, 2),
		(1, 3),
		(2, 4),
		(2, 5),
		(2, 6),
		(3, 9),
		(4, 10),
		(4, 11),
		(5, 12),
		(5, 13),
		(5, 14),
		(5, 17),
		(6, 7),
		(6, 8),
		(8, 18),
		(9, 19),
		(10, 20),
		(10, 21),
		(10, 22),
		(11, 15),
		(11, 16);
		
INSERT INTO ChannelSpecifics
VALUES	(1, 1),
		(1, 2),
		(1, 4),
		(1, 5),
		(1, 7),
		(1, 10),
		(2, 2),
		(2, 4),
		(2, 5),
		(2, 7),
		(2, 10),
		(3, 4),
		(3, 7),
		(3, 9),
		(4, 1),
		(4, 4),
		(4, 5),
		(5, 2),
		(5, 4),
		(5, 5),
		(5, 7),
		(6, 1),
		(6, 8),
		(7, 7),
		(8, 1),
		(8, 8),
		(9, 2),
		(9, 3),
		(10, 3),
		(11, 4),
		(11, 5),
		(12, 2),
		(12, 6),
		(12, 7),
		(13, 4),
		(13, 5),
		(13, 6),
		(14, 2),
		(14, 10),
		(15, 4),
		(15, 7),
		(15, 9),
		(18, 2),
		(19, 2),
		(19, 3),
		(20, 1),
		(20, 5),
		(20, 8),
		(21, 6),
		(22, 3);