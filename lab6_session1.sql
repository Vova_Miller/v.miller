/* ### READ UNCOMMITTED ### */
/* Потерянные изменения */
/*1*/ SELECT Frequency FROM Broadcasting WHERE SatelliteID = 6
/*1*/ SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
/*1*/ BEGIN TRANSACTION
/*2*/ UPDATE Broadcasting SET Frequency = Frequency + 5 WHERE SatelliteID = 6
/*4*/ COMMIT
/*5*/ SELECT Frequency FROM Broadcasting WHERE SatelliteID = 6

/* Грязное чтение */
/*1*/ SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
/*1*/ BEGIN TRANSACTION
/*2*/ SELECT Frequency FROM Broadcasting WHERE SatelliteID = 6
/*4*/ SELECT Frequency FROM Broadcasting WHERE SatelliteID = 6
/*6*/ SELECT Frequency FROM Broadcasting WHERE SatelliteID = 6
/*6*/ ROLLBACK


/* ### READ COMMITTED ### */
/* Грязное чтение */
/*1*/ SET TRANSACTION ISOLATION LEVEL READ COMMITTED
/*1*/ BEGIN TRANSACTION
/*2*/ SELECT Frequency FROM Broadcasting WHERE SatelliteID = 6
/*4*/ SELECT Frequency FROM Broadcasting WHERE SatelliteID = 6
/*6*/ SELECT Frequency FROM Broadcasting WHERE SatelliteID = 6
/*6*/ COMMIT

/* Неповторяющееся чтение */
/*1*/ SET TRANSACTION ISOLATION LEVEL READ COMMITTED
/*1*/ BEGIN TRANSACTION
/*2*/ SELECT Frequency FROM Broadcasting WHERE SatelliteID = 6
/*4*/ SELECT Frequency FROM Broadcasting WHERE SatelliteID = 6
/*4*/ ROLLBACK


/* ### REPEATABLE READ ### */
/* Неповторяющееся чтение */
/*1*/ SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
/*1*/ BEGIN TRANSACTION
/*2*/ SELECT Frequency FROM Broadcasting WHERE SatelliteID = 6
/*4*/ SELECT Frequency FROM Broadcasting WHERE SatelliteID = 6
/*4*/ COMMIT

/* Фантомы */
/*1*/ SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
/*1*/ BEGIN TRANSACTION
/*2*/ SELECT * FROM Broadcasting WHERE SatelliteID = 6
/*4*/ SELECT * FROM Broadcasting WHERE SatelliteID = 6
/*4*/ ROLLBACK


/* ### SERIALIZABLE ### */
/* Фантомы */
/*1*/ SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
/*1*/ BEGIN TRANSACTION
/*2*/ SELECT * FROM Broadcasting WHERE SatelliteID = 6
/*4*/ SELECT * FROM Broadcasting WHERE SatelliteID = 6
/*4*/ COMMIT