/* Выборки (ЛР #3) */

/* 1 */
/* Среди новостных каналов выбрать транслирующиеся на наибольшую территорию */
WITH NewsChannels AS ( SELECT Channel.ID AS ID
	FROM (Channel INNER JOIN ChannelSpecifics ON Channel.ID = ChannelID) INNER JOIN Specifics ON SpecificsID = Specifics.ID
	WHERE Description = 'News' ),
MAXIMUM AS ( SELECT MAX(ZoneTo - ZoneFrom) AS maxi FROM (NewsChannels INNER JOIN Broadcasting ON ID = ChannelID))
SELECT ID AS channel
FROM NewsChannels INNER JOIN Broadcasting ON ID = ChannelID
WHERE (ZoneTo - ZoneFrom) = (SELECT maxi FROM MAXIMUM)
ORDER BY ID;

/* 2 */
/* Выбрать спутники, радиус их орбит и расположить в порядке убывания по количеству каналов на них */
WITH SC AS ( SELECT Satellite.ID AS ID, MAX(OrbitRadius) AS rad, COUNT(*) AS cnt
	FROM (Satellite INNER JOIN Broadcasting ON Satellite.ID = SatelliteID) INNER JOIN Channel ON ChannelID = Channel.ID
	GROUP BY Satellite.ID
	UNION ALL /* каким-то спутникам не соответствовало ни одного канала */
	SELECT Satellite.ID AS ID, OrbitRadius AS rad, 0 AS cnt
	FROM Satellite LEFT JOIN Broadcasting ON Satellite.ID = SatelliteID
	WHERE SatelliteID IS NULL )
SELECT ID AS satellite, rad AS OrbitRadius
FROM SC
ORDER BY cnt DESC;

/* 3 */
/* Выбрать спутники, которые не вещают ни один канал своей страны */
SELECT DISTINCT Satellite.ID AS satellite
FROM Satellite
WHERE Satellite.CountryID != ALL(
	SELECT Company.CountryID
	FROM ((Broadcasting LEFT JOIN Channel ON ChannelID = Channel.ID) LEFT JOIN CompanyChannel ON Channel.ID = CompanyChannel.ChannelID) LEFT JOIN Company ON CompanyID = Company.ID
	WHERE SatelliteID = Satellite.ID
);

/* 4 */
/* Выбрать телекомпании, вещающие на Россию каналы развлекательной специфики (хотя бы один канал) */
WITH RussiaID AS ( SELECT ID FROM Country WHERE Name = 'Russia' ),  --id России
cID AS (  --вещаемые телеканалы развлекательной специфики
	SELECT Broadcasting.ChannelID AS ID
	FROM ((Satellite INNER JOIN Broadcasting ON Satellite.ID = SatelliteID) INNER JOIN ChannelSpecifics ON Broadcasting.ChannelID = ChannelSpecifics.ChannelID) INNER JOIN Specifics ON SpecificsID = Specifics.ID
	WHERE Description = 'Humor' )
SELECT DISTINCT Company.ID AS company
FROM (cID INNER JOIN CompanyChannel ON cID.ID = CompanyChannel.ChannelID) INNER JOIN Company ON CompanyID = Company.ID
WHERE Company.CountryID = (SELECT ID FROM RussiaID)