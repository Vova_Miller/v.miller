/* DROP TABLE positions; */
CREATE TABLE positions (
	id int NOT NULL PRIMARY KEY,
	boss_id int
);

INSERT INTO positions
VALUES	(0, 5),
		(1, 2),
		(2, 3),
		(3, 4),
		(4, 5),
		(5, 6),
		(6, 7),
		(7, 8),
		(9, 10),
		(11, 12),
		(12, 13);
	
/* Вывести всех, для кого 0 является подчинённым */
WITH ID_CTE AS (
	SELECT id, boss_id
	FROM positions
	WHERE id = 0
	
	UNION ALL

	SELECT positions.id, positions.boss_id
	FROM positions JOIN ID_CTE ON positions.id = ID_CTE.boss_id
) SELECT boss_id FROM ID_CTE

DROP TABLE positions;