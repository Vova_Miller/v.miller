CHECKPOINT
DBCC DROPCLEANBUFFERS

SET STATISTICS TIME ON
SET STATISTICS IO ON

/* Запрос #1 (c индексами) */
SELECT SatelliteID, ChannelID
FROM BroadcastingIndex
WHERE SatelliteID = 32 AND ChannelID = 64;

/* Запрос #2 (c индексами) */
SELECT SatelliteID, ChannelID, CountryID, OrbitRadius
FROM BroadcastingIndex INNER JOIN SatelliteIndex ON BroadcastingIndex.SatelliteID = SatelliteIndex.ID
WHERE ID = 32 AND ChannelID = 64;

/* Запрос #1 (без индексов) */
SELECT SatelliteID, ChannelID
FROM Broadcasting
WHERE SatelliteID = 32 AND ChannelID = 64;

/* Запрос #2 (без индексов) */
SELECT SatelliteID, ChannelID, CountryID, OrbitRadius
FROM Broadcasting INNER JOIN Satellite ON Broadcasting.SatelliteID = Satellite.ID
WHERE ID = 32 AND ChannelID = 64;

SET STATISTICS TIME OFF
SET STATISTICS IO OFF