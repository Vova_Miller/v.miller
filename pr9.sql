/* Триггеры на сроки эксплуатации спутников */
/* После любых манипуляций с таблицей Satellite удаляются все спутники с истёкшим сроком эксплуатации */

--DROP TRIGGER LimitLifeTime
CREATE TRIGGER LimitLifeTime
	ON Satellite AFTER INSERT, DELETE, UPDATE AS
		IF EXISTS( SELECT ID FROM Satellite WHERE LifeTime < CAST(getdate() AS date) )
		BEGIN
			DELETE FROM Broadcasting
			WHERE SatelliteID IN (SELECT ID FROM Satellite WHERE LifeTime < CAST(getdate() AS date))
			DELETE FROM Satellite
			WHERE LifeTime < CAST(getdate() AS date)
		END

SELECT * FROM Satellite
INSERT Satellite VALUES ((SELECT COUNT(ID) FROM Satellite) + 1, 3, 'ASU', '2016-01-05', 43999);
UPDATE Satellite SET LifeTime = '2016-01-05' WHERE ID = 7
SELECT * FROM Satellite  /* просроченных спутников в таблице нет */