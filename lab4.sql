/* Представления (ЛР #4) */

/* 1 */
/* Страна, количество спутников, количество различных каналов, вещаемых этими спутниками, количество охватываемых поясов */
SELECT Country.Name AS country, COUNT(DISTINCT Satellite.ID) AS satellites, COUNT (DISTINCT ChannelID) AS channels,
	CASE 
	WHEN MAX(ZoneTo) IS NULL OR MIN(ZoneFrom) IS NULL 
	THEN 0
	ELSE MAX(ZoneTo) - MIN(ZoneFrom) + 1
	END zones
FROM (Country LEFT JOIN Satellite ON Country.ID = Satellite.CountryID) LEFT JOIN Broadcasting ON Satellite.ID = Broadcasting.SatelliteID
GROUP BY Country.Name;

/* 2 */
/* Язык вещания, количество каналов, вещающих на этом языке, телекомпания, которая вещает наибольшее количество каналов на этом языке, количество охватываемых поясов */
SELECT ch2.Language AS language, COUNT(*) AS channels,
	(SELECT MAX(T.maxi) FROM  -- если компаний несколько, то какую-нибудь из них
		( SELECT CompanyID AS maxi
		FROM Channel INNER JOIN CompanyChannel ON Channel.language = ch2.language AND Channel.ID = CompanyChannel.ChannelID
		GROUP BY CompanyID
		HAVING COUNT(*) >= ALL(
			SELECT COUNT(*)
			FROM Channel INNER JOIN CompanyChannel ON Channel.language = ch2.language AND Channel.ID = CompanyChannel.ChannelID
			GROUP BY CompanyID ) ) AS T
	) AS company,
	ISNULL(MAX(ZoneTo) - MIN(ZoneFrom) + 1, 0) AS zones
FROM Channel AS ch2 LEFT JOIN Broadcasting ON ch2.ID = Broadcasting.ChannelID
GROUP BY ch2.Language

/* 3 */
/* Для каждого пояса и языка вещания выбрать количество каналов, вещающих на них, причем номер пояса должен быть больше '3' */
CREATE TABLE ZONES ( zone int );
INSERT INTO ZONES VALUES (1), (2), (3), (4), (5), (6), (7);
WITH T AS (
	SELECT ChannelID AS channel, Language AS language, zone
	FROM (Channel CROSS JOIN zones) FULL JOIN Broadcasting ON Channel.ID = Broadcasting.ChannelID AND (zone >= ZoneFrom) AND (zone <= ZoneTo)
	WHERE zone > 3 )
SELECT zone, language, COUNT(DISTINCT channel) AS cnt
FROM T
GROUP BY zone, language
DROP TABLE ZONES;

/* 4 */
/* Страна, количество телекомпаний, количество каналов этих телекомпаний, которые вещаются спутниками данной страны */
SELECT Country.Name AS country, companies, channels
FROM Country INNER JOIN (
	SELECT cntr.ID AS country_id, COUNT(*) AS companies,
		(SELECT COUNT(*) FROM
			( SELECT ChannelID AS channels
			FROM Satellite INNER JOIN Broadcasting ON Satellite.ID = Broadcasting.SatelliteID
			WHERE CountryID = cntr.ID
			INTERSECT
			SELECT ChannelID AS channels
			FROM Company INNER JOIN CompanyChannel ON Company.ID = CompanyChannel.CompanyID
			WHERE CountryID = cntr.ID ) AS X
		) AS channels
	FROM Country AS cntr LEFT JOIN Company ON cntr.ID = Company.CountryID
	GROUP BY cntr.ID ) AS T ON Country.ID = T.country_id



/* Изменение и удаление из базы данных (ЛР #4) */

/* Удалить все трансляции спутников, срок эксплуатации которых кончается в течение ближайших 5 лет */
DELETE FROM Broadcasting
WHERE SatelliteID IN (
	SELECT ID
	FROM Satellite
	WHERE YEAR(Lifetime) <= YEAR(getdate()) + 5 )

/* Удалить из каналов со спецификой Новости специфику Юмор */
DELETE FROM ChannelSpecifics
WHERE SpecificsID IN (
	SELECT ID
	FROM ChannelSpecifics INNER JOIN Specifics ON ChannelSpecifics.SpecificsID = Specifics.ID
	WHERE Description = 'Humor' )
AND ChannelID IN (
	SELECT ChannelID
	FROM (Channel INNER JOIN ChannelSpecifics ON Channel.ID = ChannelSpecifics.ChannelID) INNER JOIN Specifics ON ChannelSpecifics.SpecificsID = Specifics.ID
	WHERE Description = 'News' )

/* Удалить специфику телемагазина (ограничение ссылочной целостности) */
DELETE FROM Specifics
WHERE Description = 'Shop'

/* Привести имена всех спутников к образцу "ИМЯ_СТРАНЫ Sattelite" */
UPDATE Satellite
SET Name = (SELECT Name FROM Country WHERE Country.ID = Satellite.CountryID) + ' Satellite'
SELECT Name FROM Satellite