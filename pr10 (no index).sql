/* Удалить таблицы, если существовали */
IF OBJECT_ID('Broadcasting', 'U') IS NOT NULL
	DROP TABLE Broadcasting;
IF OBJECT_ID('Satellite', 'U') IS NOT NULL
	DROP TABLE Satellite;

/* Создание таблиц */
CREATE TABLE Satellite (
	ID int NOT NULL,
	CountryID int NOT NULL,
	Name varchar(64),
	LifeTime date,
	OrbitRadius int
);

CREATE TABLE Broadcasting (
	SatelliteID int NOT NULL,
	ChannelID int NOT NULL,
	Frequency int NOT NULL,  -- МГц
	ZoneFrom int NOT NULL,  -- [1, 7]
	ZoneTo int NOT NULL  -- [1, 7]
);


/* Очищение таблиц (на всякий случай) */
DELETE FROM Satellite
DELETE FROM Broadcasting;


/* Заполнение таблиц */
DECLARE @MainCount INT = 1000000   -- 100 000 ~ 00:01:48 | 1 000 000 ~ 00:18:10
DECLARE @CountriesCount INT = 197
DECLARE @iterator INT
DECLARE @mem INT
DECLARE @mem2 INT

SELECT @iterator = 1
WHILE @iterator <= @MainCount
BEGIN
	INSERT Satellite (ID, CountryID, Name, LifeTime, OrbitRadius) VALUES (
		@iterator,
		CAST(@CountriesCount * RAND() AS int) + 1,
		'Satellite ' + CAST(@iterator AS varchar),
		'20' + CAST((CAST(70 * RAND() AS int) + 20) AS varchar) + '-12-31',
		CAST(5000 * RAND() AS int) + 44000 )
    SELECT @iterator = @iterator + 1
END

SELECT @iterator = 1
WHILE @iterator <= @MainCount
BEGIN
	SELECT @mem = 0;
	SELECT @mem2 = CAST(@MainCount * RAND() AS int) + 1;
	WHILE @mem2 > @mem
	BEGIN
		INSERT INTO Broadcasting VALUES	(
			@iterator,
			@mem2,
			CAST(100 * RAND() AS int) + 50,
			CAST(4 * RAND() AS int) + 1,
			CAST(4 * RAND() AS int) + 4 )
		SELECT @mem = @mem2
		SELECT @mem2 = CAST(@MainCount * RAND() AS int) + 1;
	END
    SELECT @iterator = @iterator + 1
END