#include <iostream>
#include <fstream>
#include <unordered_set>
#include <algorithm>  // std::sort(..)
#include <cstdlib>
#include "sqlite3.h"

// Создаёт соединение и таблицу в нём.
void create() {
	// (0, 1), (0, 2), (0, 3),
	// (1, 4),
	// (2, 4), (2, 5), (2, 6), (2, 7),
	// (4, 5), (4, 8),
	// (5, 6),
	// (6, 8),
	// (8, 9), (8, 10);
	const char* SQL = "CREATE TABLE IF NOT EXISTS positions(id int NOT NULL, boss_id int NOT NULL); INSERT INTO positions VALUES (0, 1), (0, 2), (0, 3), (1, 4), (2, 4), (2, 5), (2, 6), (2, 7), (4, 5), (4, 8), (5, 6), (6, 8), (8, 9), (8, 10);";
	sqlite3 *db = 0;
	char *err = 0;
	if (sqlite3_open("hw9.db", &db) == 0)
		sqlite3_exec(db, SQL, 0, 0, &err);
	sqlite3_close(db);
}

static int callback(void *data, int argc, char **argv, char **azColName) {
	std::ofstream temp("temp", std::ios_base::app);
	temp << argv[0] << " ";
	temp.close();
	return 0;
}

void i(int ID, int k) {
	sqlite3 *db = 0;
	char *err = 0;
	std::ifstream temp;
	std::ofstream cleaner;
	int w, v;
	char int_string[16];
	std::vector<int> IDs;  // Для упорядоченного вывода всех ID.
	std::unordered_set<int> overviewed;  // Хэш-таблица уже проверенных id.
	std::unordered_set<int> to_view;  // Хэш-таблица id, которые на следующем шагу нужно будет рассмотреть.
	std::unordered_set<int> buffer;

	to_view.insert(ID);
	for (w = 0; w < k; ++w) {
		buffer.clear();
		for (std::unordered_set<int>::iterator it = to_view.begin(); it != to_view.end(); ++it) {
			// SELECT boss_id FROM positions WHERE id = ?;
			// Формирование запроса.
			char select_query[64] = "SELECT boss_id FROM positions WHERE id = ";
			sprintf_s(int_string, "%d", *it);
			strcat_s(select_query, int_string);
			strcat_s(select_query, ";");

			// Очистить/создать временный файл.
			cleaner.open("temp", std::ofstream::out | std::ofstream::trunc);
			cleaner.close();

			// Выполнить запрос, его результат выписывается в файл.
			if (sqlite3_open("hw9.db", &db) == 0)  // Открываем соединение.
				if (sqlite3_exec(db, select_query, callback, 0, &err)) {  // Выполняем запрос.
					fprintf(stderr, "Ошибка: %s", err);  // Ошибка.
					sqlite3_free(err);
				}
			sqlite3_close(db);  // Закрываем соединение.

			// Читаем, что выдал запрос.
			temp.open("temp");
			while (!temp.eof()) {
				temp >> v;
				if ((overviewed.find(v) == overviewed.end()) && (to_view.find(v) == to_view.end()) && (buffer.find(v) == buffer.end()))
					buffer.insert(v);
			}
			temp.close();
			std::remove("temp");
		}
		for (std::unordered_set<int>::iterator it = to_view.begin(); it != to_view.end(); ++it)
			overviewed.insert(*it);
		to_view = buffer;
	}

	// Оставшиеся ID выкидываются в уже просмотренные.
	for (std::unordered_set<int>::iterator it = to_view.begin(); it != to_view.end(); ++it)
		overviewed.insert(*it);

	// Все просмотренные ID выкидываются в вектор.
	for (std::unordered_set<int>::iterator it = overviewed.begin(); it != overviewed.end(); ++it)
		if ((*it) != ID)
			IDs.push_back(*it);

	// Вектор сортируется и выводится.
	std::sort(IDs.begin(), IDs.end());
	for (std::vector<int>::iterator it = IDs.begin(); it != IDs.end(); ++it)
			std::cout << *it << " ";
	std::cout << std::endl;
}

int main(int argc, char **argv) {
	int n, k;
	setlocale(LC_ALL, "");  // Для корректного отображения русского текста.
	std::cin >> n >> k;
	i(n, k);
	system("pause");
	return 0;
}