/* 16 */
SELECT DISTINCT A.model, B.model, A.speed, A.ram
FROM PC AS A, PC AS B
WHERE (A.model > B.model) AND (A.speed = B.speed) AND (A.ram = B.ram)

/* 17 */
SELECT DISTINCT Product.type, Laptop.model, Laptop.speed
FROM Product INNER JOIN Laptop ON Product.model = Laptop.model
WHERE Laptop.speed < ALL(SELECT PC.speed FROM PC)

/* 18 */
SELECT DISTINCT Product.maker, Printer.price
FROM Product INNER JOIN Printer ON Product.model = Printer.model
WHERE (Printer.color = 'y') AND Printer.price IN (SELECT MIN(price) FROM Printer WHERE color = 'y')

/* 19 */
SELECT maker, AVG(screen)
FROM Product INNER JOIN Laptop ON Product.model = Laptop.model
GROUP BY maker

/* 20 */
SELECT maker, COUNT(type)
FROM Product
WHERE type = 'PC'
GROUP BY maker
HAVING COUNT(type) >= 3

/* 21 */
SELECT maker, MAX(price)
FROM Product INNER JOIN PC ON Product.model = PC.model
GROUP BY maker

/* 22 */
SELECT speed, AVG(price)
FROM PC
WHERE speed > 600
GROUP BY speed

/* 23 */
SELECT DISTINCT maker
FROM Product INNER JOIN PC ON Product.model = PC.model
WHERE speed >= 750
INTERSECT
SELECT DISTINCT maker
FROM Product INNER JOIN Laptop ON Product.model = Laptop.model
WHERE speed >= 750

/* 24 */
WITH everything AS (
SELECT PC.model, PC.price
FROM PC
UNION
SELECT Laptop.model, Laptop.price
FROM Laptop
UNION
SELECT Printer.model, Printer.price
FROM Printer )
SELECT model
FROM everything
WHERE price IN (SELECT MAX(price) FROM everything)

/* 25 */
WITH MinRam AS (
SELECT model, speed
FROM PC
WHERE ram IN (SELECT MIN(ram) FROM PC) )
SELECT DISTINCT maker
FROM Product
WHERE (type = 'printer') AND (maker IN ( SELECT maker
	FROM Product INNER JOIN MinRam ON Product.model = MinRam.model
	WHERE speed IN (SELECT MAX(speed) FROM MinRam) ))
	
/* 26 */
SELECT AVG(price)
FROM ( SELECT price
	FROM Product INNER JOIN PC ON Product.model = PC.model
	WHERE maker = 'A'
	UNION ALL
	SELECT price
	FROM Product INNER JOIN Laptop ON Product.model = Laptop.model
	WHERE maker = 'A' ) X
	
/* 27 */
SELECT maker, AVG(hd)
FROM Product INNER JOIN PC ON Product.model = PC.model
WHERE maker IN (SELECT maker FROM Product WHERE type = 'Printer')
GROUP BY maker