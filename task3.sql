/* 28 */
SELECT CAST(((V1.t_sum*1.0) / (V2.t_count*1.0)) AS NUMERIC(6,2))
FROM (SELECT (CASE WHEN COUNT(*) = 0 THEN 0 ELSE SUM(B_VOL) END) AS t_sum FROM utB) AS V1, (SELECT COUNT(Q_ID) AS t_count FROM utQ) AS V2

/* 29 */
SELECT (CASE
	WHEN Income_o.point IS NULL
	THEN Outcome_o.point
	ELSE Income_o.point
	END),
	(CASE
	WHEN Income_o.date IS NULL
	THEN Outcome_o.date
	ELSE Income_o.date
	END),
	Income_o.inc, Outcome_o.out
FROM Income_o FULL JOIN Outcome_o ON (Income_o.point = Outcome_o.point) AND (Income_o.date = Outcome_o.date)

/* 30 */
WITH Income2 AS ( SELECT point, date, SUM(inc) AS inc2
FROM Income
GROUP BY point, date ),
Outcome2 AS ( SELECT point, date, SUM(out) AS out2
FROM Outcome
GROUP BY point, date )
SELECT (CASE
	WHEN Income2.point IS NULL
	THEN Outcome2.point
	ELSE Income2.point
	END),
	(CASE
	WHEN Income2.date IS NULL
	THEN Outcome2.date
	ELSE Income2.date
	END),
	(CASE
	WHEN Outcome2.out2 = 0
	THEN NULL
	ELSE Outcome2.out2
	END),
	(CASE
	WHEN Income2.inc2 = 0
	THEN NULL
	ELSE Income2.inc2
	END)
FROM Income2 FULL JOIN Outcome2 ON (Income2.point = Outcome2.point) AND (Income2.date = Outcome2.date)

/* 31 */
SELECT class, country
FROM Classes
WHERE bore >= 16

/* 32 */
WITH Classes2 AS ( SELECT Classes.class AS class2, name FROM Classes JOIN Ships ON Classes.class = Ships.class
	UNION
	SELECT class AS class2, class FROM Classes JOIN Outcomes ON Classes.class = Outcomes.ship )
SELECT country, CAST(AVG(((bore*bore*bore*1.0) / 2)) AS NUMERIC(6,2)) AS weight
FROM Classes JOIN Classes2 ON Classes.class = Classes2.class2
GROUP BY country

/* 33 */
SELECT ship
FROM Outcomes
WHERE (battle = 'North Atlantic') AND (result = 'sunk')

/* 34 */
SELECT name
FROM Classes JOIN Ships ON Classes.class = Ships.class
WHERE (launched >= 1922) AND (type = 'bb') AND (displacement > 35000)

/* 35 */
SELECT model, type
FROM Product
WHERE (model LIKE '%[0-9]%' AND model NOT LIKE '%[^0-9]%') OR
	(model LIKE '%[A-Z]%' AND model NOT LIKE '%[^A-Z]%')
	
/* 36 */
SELECT name AS headers
FROM Ships
WHERE name = class
UNION
SELECT ship AS headers
FROM Classes JOIN Outcomes ON Classes.class = Outcomes.ship

/* 37 */
WITH Classes2 AS ( SELECT Classes.class AS class2, name AS name2
	FROM Classes JOIN Ships ON Classes.class = Ships.class
	UNION
	SELECT class AS class2, class AS name2
	FROM Classes JOIN Outcomes ON Classes.class = Outcomes.ship )
SELECT class2
FROM Classes2
GROUP BY class2
HAVING COUNT(*) = 1

/* 38 */
SELECT DISTINCT country
FROM Classes
WHERE type = 'bb'
INTERSECT
SELECT DISTINCT country
FROM Classes
WHERE type = 'bc'

/* 39 */
WITH DShips AS ( SELECT ship, MIN(date) AS year
	FROM Outcomes JOIN Battles ON Outcomes.battle = Battles.name
	WHERE result = 'damaged'
	GROUP BY ship )
SELECT DISTINCT DShips.ship
FROM DShips
WHERE EXISTS( SELECT ship
	FROM ( Outcomes JOIN Battles ON Outcomes.battle = Battles.name )
	WHERE (ship = DShips.ship) AND (date > DShips.year) )

/* 40 */
SELECT Ships.class, name, country
FROM Classes JOIN Ships ON Classes.class = Ships.class
WHERE numGuns >= 10

/* 41 */
SELECT char, value
FROM ( SELECT CAST(model AS CHAR(25)) AS model,
		CAST(speed AS CHAR(25)) AS speed,
		CAST(ram AS CHAR(25)) AS ram,
		CAST(hd AS CHAR(25)) AS hd,
		CAST(cd AS CHAR(25)) AS cd,
		CAST(price AS CHAR(25)) AS price
	FROM PC
	WHERE code IN ( SELECT MAX(code) FROM PC ) ) X
UNPIVOT
(value FOR char IN (model, speed, ram, hd, cd, price)) unpvt

/* 42 */
SELECT ship, battle
FROM Outcomes
WHERE result = 'sunk'

/* 43 */
SELECT name
FROM Battles
WHERE DATEPART(yyyy, date) NOT IN ( SELECT launched FROM Ships WHERE launched IS NOT NULL )

/* 44 */
SELECT name AS Rname
FROM Ships
WHERE name LIKE 'R%'
UNION
SELECT ship AS Rname
FROM Outcomes
WHERE ship LIKE 'R%'

/* 45 */
SELECT name AS name3
FROM Ships
WHERE name LIKE '% % %'
UNION
SELECT ship AS name3
FROM Outcomes
WHERE ship LIKE '% % %'
