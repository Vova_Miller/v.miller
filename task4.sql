/* 46 */
SELECT DISTINCT ship, displacement, numGuns
FROM (Classes JOIN Ships ON Classes.class = Ships.class) RIGHT JOIN Outcomes ON ((Ships.name = Outcomes.ship) OR (Classes.class = Outcomes.ship))
WHERE battle = 'Guadalcanal'


/* 47 */
WITH T AS ( SELECT COUNT(*) AS cnt, maker
	FROM Product
	GROUP BY maker )
SELECT COUNT(*) + (SELECT ISNULL(SUM(T2.cnt), 0)
	FROM T AS T2
	WHERE (T2.cnt > (SELECT T.cnt FROM T WHERE T.maker = P2.maker)) OR (T2.cnt = (SELECT T.cnt FROM T WHERE T.maker = P2.maker) AND T2.maker < P2.maker)
) AS num, P2.maker, P2.model
FROM Product AS P1 INNER JOIN Product AS P2 ON (P1.maker = P2.maker AND P1.model <= P2.model)
GROUP BY P2.model, P2.maker


/* 48 */
SELECT DISTINCT Classes.class
FROM (Outcomes JOIN Ships ON Outcomes.ship = Ships.name) JOIN Classes ON Ships.class = Classes.class
WHERE result = 'sunk'

UNION

SELECT DISTINCT Classes.class
FROM Outcomes JOIN Classes ON Outcomes.ship = Classes.class
WHERE result = 'sunk'


/* 49 */
SELECT name AS name
FROM Ships JOIN Classes ON Ships.class = Classes.class
WHERE bore = 16

UNION

SELECT ship AS name
FROM Outcomes JOIN Classes ON Outcomes.ship = Classes.class
WHERE bore = 16


/* 50 */
SELECT DISTINCT battle
FROM Outcomes JOIN Ships ON Outcomes.ship = Ships.name
WHERE class = 'Kongo'


/* 51 */
SELECT name AS name
FROM Ships JOIN Classes AS C1 ON Ships.class = C1.class
WHERE C1.numGuns >= ALL( SELECT C2.numGuns
	FROM Ships JOIN Classes AS C2 ON Ships.class = C2.class
	WHERE C1.displacement = C2.displacement
	UNION
	SELECT C2.numGuns
	FROM Outcomes JOIN Classes AS C2 ON Outcomes.ship = C2.class
	WHERE C1.displacement = C2.displacement )

UNION

SELECT ship AS name
FROM Outcomes JOIN Classes AS C1 ON Outcomes.ship = C1.class
WHERE C1.numGuns >= ALL( SELECT C2.numGuns
	FROM Ships JOIN Classes AS C2 ON Ships.class = C2.class
	WHERE C1.displacement = C2.displacement
	UNION
	SELECT C2.numGuns
	FROM Outcomes JOIN Classes AS C2 ON Outcomes.ship = C2.class
	WHERE C1.displacement = C2.displacement )


/* 52 */
SELECT name
FROM Ships JOIN Classes ON Ships.class = Classes.class
WHERE (ISNULL(type, 'bb') = 'bb') AND (ISNULL(country, 'Japan') = 'Japan') AND (ISNULL(numGuns, 9) >= 9) AND (ISNULL(bore, 18) < 19) AND (ISNULL(displacement, 65000) <= 65000)


/* 53 */
SELECT CAST(AVG(numGuns*1.0) AS NUMERIC(6,2))
FROM Classes
WHERE type = 'bb'


/* 54 */
WITH T AS ( SELECT name AS name, numGuns AS guns
	FROM Ships JOIN Classes ON Ships.class = Classes.class
	WHERE type = 'bb'
	UNION
	SELECT ship AS name, numGuns AS guns
	FROM Outcomes JOIN Classes ON Outcomes.ship = Classes.class
	WHERE type = 'bb' )
SELECT CAST(AVG(guns*1.0) AS NUMERIC(6,2))
FROM T


/* 55 */
SELECT Classes.class AS class, MIN(Ships.launched) AS year
FROM Classes JOIN Ships ON Classes.class = Ships.class
GROUP BY Classes.class
UNION
SELECT Classes.class AS class, NULL AS year
FROM Classes
WHERE class NOT IN ( SELECT class FROM Ships )


/* 56 */
WITH T AS ( SELECT Ships.name AS name, Classes.class AS class
	FROM (Classes JOIN Ships ON Classes.class = Ships.class) JOIN Outcomes ON Ships.name = Outcomes.ship
	WHERE result = 'sunk'
	UNION
	SELECT Outcomes.ship AS name, Classes.class AS class
	FROM Classes JOIN Outcomes ON Classes.class = Outcomes.ship
	WHERE result = 'sunk' )
SELECT class, COUNT(*) AS count
FROM T
GROUP BY class
UNION
SELECT class, 0 AS count
FROM Classes
WHERE class NOT IN (SELECT class FROM T)


/* 57 */
WITH TSunk AS ( SELECT Ships.name AS name, Classes.class AS class
	FROM (Classes JOIN Ships ON Classes.class = Ships.class) JOIN Outcomes ON Ships.name = Outcomes.ship
	WHERE result = 'sunk'
	UNION
	SELECT Outcomes.ship AS name, Classes.class AS class
	FROM Classes JOIN Outcomes ON Classes.class = Outcomes.ship
	WHERE result = 'sunk' ),
T AS ( SELECT Ships.name AS name, Classes.class AS class
	FROM Classes JOIN Ships ON Classes.class = Ships.class
	UNION
	SELECT Outcomes.ship AS name, Classes.class AS class
	FROM Classes JOIN Outcomes ON Classes.class = Outcomes.ship )
SELECT TSunk.class AS class, COUNT(*) AS count
FROM TSunk
GROUP BY TSunk.class
HAVING TSunk.class IN (SELECT T.class AS class FROM T GROUP BY T.class HAVING COUNT(*) >= 3)


/* 58 */
SELECT M.maker, T.type, CAST(CAST(100*(SELECT COUNT(*) FROM Product WHERE Product.maker = M.maker AND Product.type = T.type) AS NUMERIC(6,2)) / CAST((SELECT COUNT(*) FROM Product WHERE Product.maker = M.maker) AS NUMERIC(6,2)) AS NUMERIC(6,2)) AS percentage
FROM (SELECT DISTINCT maker FROM Product) AS M, (SELECT DISTINCT type FROM Product) AS T


/* 59 */
SELECT point, SUM(money)
FROM ((SELECT point, inc AS money FROM Income_o) UNION ALL (SELECT point, (-1)*out AS money FROM Outcome_o)) AS X
GROUP BY point


/* 60 */
SELECT point, SUM(money)
FROM ((SELECT point, inc AS money FROM Income_o WHERE date < '2001-04-15') UNION ALL (SELECT point, (-1)*out AS money FROM Outcome_o WHERE date < '2001-04-15')) AS X
GROUP BY point

